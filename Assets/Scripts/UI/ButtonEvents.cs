﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;


namespace BOZP.UI
{
 public class ButtonEvents : MonoBehaviour
 {
  public string[] levels;
  int CurrentLevel = 0;
  public TextMeshPro LevelText;
  
  public void SelectNextLevel()
  {
   if (CurrentLevel < levels.Length - 1)
    CurrentLevel++;
   else if (CurrentLevel == levels.Length - 1)
    CurrentLevel = 0;
   LevelText.text = levels[CurrentLevel];
  }

  public void SelectPrevLevel()
  {
   if (CurrentLevel > 0)
    CurrentLevel--;
   else if (CurrentLevel == 0)
    CurrentLevel = levels.Length;
   LevelText.text = levels[CurrentLevel];
  }

  public void QuitGame()
  {
   Application.Quit();
   print("Quit game");
  }

  public void LoadLevel()
  {
   SceneManager.LoadScene(levels[CurrentLevel]);
  }
 }
}