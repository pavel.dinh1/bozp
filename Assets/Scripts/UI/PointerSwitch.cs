﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
using BOZP;

namespace BOZP.UI
{
 public class PointerSwitch : MonoBehaviour
 {
  [SerializeField] GameObject m_Pointer;
  [SerializeField] VRInputModule m_Module;
  [SerializeField] SteamVR_Input_Sources s_RightHand;
  [SerializeField] SteamVR_Input_Sources s_LeftHand;
  [SerializeField] SteamVR_Action_Boolean clickAction;

  //TODO find out how to set instance after start the game
  [SerializeField] Hand r_Hand;
  [SerializeField] Hand l_Hand;

  // Get the player to put it on
  [SerializeField] GameObject player;
  [SerializeField] PlayerSpawn spawner;
  public GameObject Player { get => Player; set => Player = value; }

  private void Start()
  {
   // TODO find object of the hand
   //spawner = GetComponent<PlayerSpawn>();
   //Player = spawner.Player;
  }

  private void Update()
  {
   if (clickAction.GetStateDown(s_RightHand))
   {
    m_Pointer.transform.parent = r_Hand.transform;
   }
   if (clickAction.GetStateDown(s_LeftHand))
   {
    m_Pointer.transform.parent = l_Hand.transform;
   }
  }
 }
}