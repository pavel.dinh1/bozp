﻿using BOZP.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BOZP
{
 public class PlayerSpawn : MonoBehaviour
 {
  [SerializeField] private GameObject player;

  public GameObject Player { get => player; set => player = value; }

  void Awake()
  {
   if (Player != null)
   {
    Instantiate(Player, transform.position, transform.rotation);
    Player.transform.Translate(0, 0, 0);
   }
  }
 }
}