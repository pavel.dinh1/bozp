﻿namespace BOZP.Interactions
{
    [System.Serializable]
    public enum BodyPart
    {
        Hlava,
        LeveRameno,
        PraveRameno,
        PravaHorniPaze,
        LevaHorniPaze,
        PravaSpodniPaze,
        LevaSpodniPaze,
        PravaRuka,
        LevaRuka,
        Stred,
        PraveStehno,
        LeveStehno,
        PravaSpodniNoha,
        LevaSpodniNoha,
        PraveChodidlo,
        LeveChodidlo
    }

    [System.Serializable]
    public class BodyState
    {
        public bool Bleeding;
        public bool Broken;
        public bool Bruised;
    }
}