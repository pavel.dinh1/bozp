﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using BOZP.Interactions;
using UnityEngine.SceneManagement;

namespace EventSystem
{
 public class HealthStatus : MonoBehaviour
 {
  // ZATIM NIC NEDELA
  public delegate void OnHumanDied();
  public event OnHumanDied onHumanDied;

  Slider bloodStatus = null;
  [SerializeField] TextMeshPro bloodStatusText = null;
  TextMeshProUGUI bloodCapText = null;
  BodyContact[] bodyStatus;
  //int bodyContacts;
  float bloodCapacity;

  string gameNotification = "Your patient lost too much blood";
  [SerializeField] float TotalBlood = 7.4f;
  [SerializeField] GameObject person = null;
  [SerializeField] float bloodDrain;

  void Start()
  {
   bloodStatus = GetComponentInChildren<Slider>();
   bloodStatus.value = 1;

   bloodStatusText = GetComponentInChildren<TextMeshPro>();
   bloodCapText = GetComponentInChildren<TextMeshProUGUI>();

   bloodCapacity = TotalBlood;
   bodyStatus = person.GetComponentsInChildren<BodyContact>();
  }

  void Update()
  {
   BloodDrain();
   if (bloodDrain == 0)
   {
    gameNotification = "Gratuluji zachranil jste zivot";
    bloodStatusText.text = gameNotification;
    SceneManager.LoadScene("MainMenu");
   }
  }

  // Pricitej ztratu krve podle poctu casti ktere krvaci
  void CalculateBloodDrain()
  {
   for (int i = 0; i < bodyStatus.Length; i++)
   {
    IncreaseBloodLost(i);
    DecreaseBloodLost(i);
    //ChangeBloodLost(bodyStatus[i].stav.Bleeding, i);
   }
  }

  // odcitani krve
  private void DecreaseBloodLost(int i)
  {
   if (!bodyStatus[i].stav.Bleeding && bloodDrain > 0)
   {
    if (bodyStatus[i].currentlyBleeding)
    {
     bloodDrain -= bodyStatus[i].bloodLostIncrease;
     bodyStatus[i].currentlyBleeding = false;
    }
   }
  }

  private void IncreaseBloodLost(int i)
  {
   if (bodyStatus[i].stav.Bleeding && bloodDrain <= 0)
   {
    // Jestlize uz krvaci tak nepricitej ztratu krve
    if (!bodyStatus[i].currentlyBleeding)// promena
    {
     bloodDrain += bodyStatus[i].bloodLostIncrease;
     bodyStatus[i].currentlyBleeding = true;
    }
   }
  }

  // Prevod kalkulace na UI
  void BloodDrain()
  {
   if (bloodStatus.value > 0)
   {
    CalculateBloodDrain(); // Spocitej kolik casti tela krvaci
    TransferToUI(); // Spocitej z procentualniho do ciselneho objemu krve a pridej do UI
   }

   // Clovek umrel pokud ztratil moc krve
   if (bloodStatus.value == 0)
   {
    // zabit cloveka
    Die();
    bloodStatusText.text = gameNotification;
   }

  }

  private void TransferToUI()
  {
   float drainAmount = bloodDrain * Time.deltaTime;
   /* Prevod slider statusu do kapacity krve v tele (bloodCapacity) */
   bloodStatus.value -= drainAmount;
   bloodCapacity = bloodStatus.value * TotalBlood;
   bloodCapText.text = bloodCapacity.ToString("0.000" + "L");
  }

  void Die()
  {
   Destroy(person.gameObject);
   //Vyvolej event
   if (onHumanDied != null)
   {
    // TODO vysli do score systemu ze clovek umrel
    onHumanDied();
   }
  }
 }
}