﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

namespace BOZP.Interactions
{
 public class BodyContact : MonoBehaviour
 {
  [SerializeField] string tagFilter = "Interactable";
  [SerializeField] TextMesh textMesh = null;

  Item item;

  public BodyState stav;
  public BodyPart bodyPart;
  public float bloodLostIncrease = 0.002f;

  [HideInInspector]
  public bool wasHeld;

  [HideInInspector]
  public bool currentlyBleeding;

  private void OnTriggerEnter(Collider other)
  {
   Bleeding(other);
  }
  
  private void OnTriggerStay(Collider other)
  {
   // Checking state if is valid with the enum state to destroy
   CheckDestroy(other);
  }
  
  private void OnTriggerExit(Collider other)
  {
   wasHeld = false;
   textMesh.gameObject.GetComponent<MeshRenderer>().enabled = false;
  }

  //Krvaceni
  private void Bleeding(Collider other)
  {
   if (stav.Bleeding)
   {
    IsItemCheck(other);
   }
  }
  
  public void CheckDestroy(Collider other)
  {
   if (other.gameObject.tag == tagFilter)
   {
    if (ItemCompatible(item, other))
    {
     DestroyItem(other);
    }
   }
  }

  public void ShowText()
  {
   textMesh.gameObject.GetComponent<MeshRenderer>().enabled = true;
  }

  public void HideText()
  {
   textMesh.gameObject.GetComponent<MeshRenderer>().enabled = false;
  }

  void IsItemCheck(Collider other)
  {
   if (other.GetComponent<Item>())
   {
    item = other.GetComponent<Item>();
    item.HasInteract(true);
    textMesh.gameObject.GetComponent<MeshRenderer>().enabled = true;
    if (item.IsHolding())
    {
     wasHeld = true;
    }
   }
  }

  // Zkontroluj jestli pasuje item k enum listu
  bool ItemCompatible(Item otherItem, Collider other)
  {
   if (otherItem == null)
   {
    return false;
   }
   for (int x = 0; x < otherItem.triggerCheck.Length; x++)
   {
    if (bodyPart.Equals(otherItem.triggerCheck[x]))
    {
     return true;
    }
   }
   return false;
  }

  // Znic objekt pokud probehla interakce
  void DestroyItem(Collider other)
  {
   if (wasHeld && !item.IsHolding())
   {
    stav.Bleeding = false;
    item.HasInteract(false);
    textMesh.GetComponent<MeshRenderer>().enabled = false;
    Destroy(other.gameObject);
   }
  }
 }
}
