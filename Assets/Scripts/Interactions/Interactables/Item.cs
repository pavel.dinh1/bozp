﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

namespace BOZP.Interactions
{
 public class Item : MonoBehaviour
 {
  [SerializeField] private TextMesh textName = null;
  private Interactable interactable;
  public BodyPart[] triggerCheck;

  private void Start()
  {
   interactable = GetComponent<Interactable>();
  }

  public void ShowTextUI()
  {
   if (textName != null)
   {
     textName.gameObject.GetComponent<MeshRenderer>().enabled = true;
   }
  }

  public void HideTextUI()
  {
   if (textName != null)
   {
    textName.gameObject.GetComponent<MeshRenderer>().enabled = false;
   }
  }

  public bool IsHolding()
  {
   return interactable.attachedToHand;
  }

  public bool HasInteract(bool isIn)
  {
   return isIn;
  }
 }
}