﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class TeleprtableArea : MonoBehaviour
{
 [SerializeField] TeleportArea[] tparea;

 private void Awake()
 {
  tparea = GetComponentsInChildren<TeleportArea>();
 }

 public void UnlockArea()
 {
  foreach(TeleportArea ta in tparea)
  {
   ta.locked = false;
  }
 }
}
