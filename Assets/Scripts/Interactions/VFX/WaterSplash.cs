﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterSplash : MonoBehaviour
{
 ParticleSystem[] waterSystem;

 private void Start()
 {
  waterSystem = GetComponentsInChildren<ParticleSystem>();
 }

 public void OpenSplash()
 {
  foreach (ParticleSystem ps in waterSystem)
  {
   ps.Play();
  }
 }

}
