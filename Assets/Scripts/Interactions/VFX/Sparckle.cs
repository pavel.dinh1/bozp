﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sparckle : MonoBehaviour
{
 ParticleSystem ps;

 private void Start()
 {
  ps = GetComponent<ParticleSystem>();
 }
 public void StopSpawning()
 {
  ps.Stop();
  if (ps.GetComponentInChildren<Light>() != null)
  {
   ps.GetComponentInChildren<Light>().enabled = false;
  }
 }
}
