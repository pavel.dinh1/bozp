﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookingText : MonoBehaviour
{
    MeshRenderer textMesh;
    //string text;
    [SerializeField] Transform playerCam = null;

    private void Start()
    {
        textMesh = GetComponent<MeshRenderer>();
        textMesh.enabled = false;
    }

    private void Update()
    {
        if (playerCam)
        {
            transform.LookAt(playerCam.transform.position);
            transform.Rotate(0, 180, 0);
        }
    }
}
