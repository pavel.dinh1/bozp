﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowText : MonoBehaviour
{
    [SerializeField] Transform target = null;

    // Update is called once per frame
    void Update()
    {
        transform.position = target.transform.position + new Vector3(0, .1f);
    }
    
}
