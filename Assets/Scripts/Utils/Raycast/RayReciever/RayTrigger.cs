﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayTrigger : MonoBehaviour
{
    [SerializeField] protected float HitByRayRefreshTime = 0.09f;
    [SerializeField] protected bool isHit = false;
    protected float RayRunsOutTime;

    public virtual void HitRecieved()
    {
        // Do something after hit
    }
    protected void SetTimer()
    {
        isHit = true;
        RayRunsOutTime = Time.time + HitByRayRefreshTime;
    }
}
