﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionTrigger : RayTrigger
{
    ColorBlock buttonColor;
    Renderer shader;

    [SerializeField] Material HighlightColor = null;
    [SerializeField] Material ClickedColor;
    [SerializeField] Material BaseColor = null;
    [SerializeField] string description = "";
    [SerializeField] TextMesh text = null;
    [SerializeField] float fadeTime = 0.09f;



    void Start()
    {
        shader = GetComponent<Renderer>();
        text.color = Color.clear;
    }
    void Update()
    {
        if (Time.time > RayRunsOutTime)
        {
            isHit = false;
        }
        ShowDescription();
        ResetColor();
        ButtonClicked();
    }
    void ResetColor()
    {
        if (!isHit)
        {
            ChangeColor(BaseColor);
        }
    }
    public override void HitRecieved()
    {
        SetTimer();
        ChangeColor(HighlightColor);
    }

    void ChangeColor(Material colorToChange)
    {
        shader.material = colorToChange;
    }

    void ShowDescription()
    {
        if (isHit)
        {
            text.text = description;
            text.color = Color.Lerp(text.color, Color.white, fadeTime * Time.deltaTime);
        }
        else
        {
            text.color = Color.Lerp(text.color, Color.clear, fadeTime * Time.deltaTime);
        }
    }
    void ButtonClicked()
    {
        //TODO - replace with VR input
        if (isHit && Input.GetMouseButton(0)) // Check if input was called 
        {
            print("clicked!");
        }
    }
}
