﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Raycast : MonoBehaviour
{
    // Base params
    [SerializeField] protected Color colorBase;
    [SerializeField] protected Color onColorHit;
    [SerializeField] protected float distance = 10f;
    [SerializeField] LayerMask layer = 1;

    //Raycast
    RaycastHit hit;
    public RaycastHit Hit { get { return hit; } }

    void Update()
    {
        DetectHit();
    }

    protected virtual void DetectHit()
    {
        Ray ray = new Ray(transform.position, transform.forward);
        if (Physics.Raycast(ray, out hit, distance, layer))
        {
            RayTrigger t = hit.collider.gameObject.GetComponent<RayTrigger>();
            if (t != null)
            {
                t.HitRecieved();
            }
        }
    }

    protected void DebugDrawRay()
    {
        if (hit.collider)
        {
            Debug.DrawRay(transform.position, transform.forward * distance, onColorHit);
        }
        else
        {
            Debug.DrawRay(transform.position, transform.forward * distance, colorBase);
        }
    }
}
